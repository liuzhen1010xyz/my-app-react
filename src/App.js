import logo from './logo.svg';
import { Link } from "react-router-dom";
import './App.css';
import {
  Routes,
  Route,
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          <Link style={{marginRight: '10px', color: '#fff'}} to="/">Home</Link>
          <Link style={{marginRight: '10px', color: '#fff'}} to="/about">About Me</Link>
          <Link style={{marginRight: '10px', color: '#fff'}} to="/list">Go to list</Link>
          <a href='/'>Go to multipage app</a>
        </p>
        <Routes>
          <Route path="/" element={<div>
            <img src={logo} className="App-logo" alt="logo" />
            <p>
              Edit <code>src/App.js</code> and save to reload.(change)
            </p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </div>} />
          <Route path="/about" element={<p>About Me Page</p>} />
          <Route path="/list" element={<ul><li>学习react</li><li>学习docker</li></ul>} />
          <Route path="*" element={<div>
            <h2>404 Not found</h2>
            <Link to="/">Go Home</Link>
          </div>} />
        </Routes>
      </header>
    </div>
  );
}

export default App;
